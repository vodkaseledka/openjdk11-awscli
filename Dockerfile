FROM openjdk:11

LABEL maintainer="alexander.smirnoff@gmail.com"

RUN set -eux; \
        apt-get update; \
        apt-get install -y --no-install-recommends \
            python3-setuptools \
            python3-pip \
        ; \
        rm -rf /var/lib/apt/lists/*

RUN pip3 --no-cache-dir install -U awscli

RUN apt-get clean
